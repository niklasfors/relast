public class AbstractTests {
	protected void assertException() {
		check(false, "should throw exception");
	}
	protected void assertTrue(boolean b) {
		check(b, "value should be true (is false)");
	}
	protected void assertFalse(boolean b) {
		check(!b, "value should be flase (is true)");
	}
	protected void assertNull(Object obj) {
		check(obj == null, "Object not null: " + obj);
	}
	protected void assertSame(Object o1, Object o2) {
		check(o1 == o2, "Objects not same: " + o1 + ", " + o2);
	}
	protected void assertEquals(Object o1, Object o2) {
		check(o1.equals(o2), "Objects not equals: " + o1 + ", " + o2);
	}
	protected void check(boolean b, String message) {
		if (!b) {
			throw new RuntimeException(message);
		}
	}
}