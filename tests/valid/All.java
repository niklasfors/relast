import AST.*;
import java.util.*;

public class All extends AbstractTests {
	private Root r;
	private A a1;
	private A a2;
	private A a3;
	private B b1;
	private B b2;
	private B b3;

	public static void main(String args[]) {
		new All().test();
	}

	public void test() {
		testDi1();
		testDi2();
		testDi3();

		testBi1();
		testBi2();
		testBi3();
		testBi4();
		testBi5();
		testBi6();
		testBi7();
		testBi8();
		testBi9();

		testImmutableList();
	}

	/**
	 * rel A.di1 -> B;
	 */
	private void testDi1() {
		setup();
		a1.setDi1(b2);
		a2.setDi1(b1);

		assertSame(a1.di1(), b2);
		assertSame(a2.di1(), b1);

		a2.setDi1(b2);

		assertSame(a1.di1(), b2);
		assertSame(a2.di1(), b2);

		try {
			a3.setDi1(null);
			assertException();
		} catch (Exception e) {
			// OK
		}
	}


	/**
	 * rel A.di2? -> B;
	 */
	private void testDi2() {
		setup();
		a1.setDi2(b2);
		a2.setDi2(b1);

		assertSame(a1.di2(), b2);
		assertSame(a2.di2(), b1);

		a2.setDi2(b2);

		assertSame(a1.di2(), b2);
		assertSame(a2.di2(), b2);

		a2.clearDi2();

		assertSame(a1.di2(), b2);
		assertNull(a2.di2());

		assertTrue(a1.hasDi2());
		assertFalse(a2.hasDi2());
		assertFalse(a3.hasDi2());
	}


	/**
	 * rel A.di3* -> B;
	 */
	private void testDi3() {
		setup();
		a1.addToDi3(b1);
		a1.addToDi3(b2);
		a1.addToDi3(b3);
		a2.addToDi3(b2);
		
		assertEquals(a1.di3(), Arrays.asList(b1, b2, b3));
		assertEquals(a2.di3(), Arrays.asList(b2));
		assertEquals(a3.di3(), Arrays.asList());

		a1.addToDi3(b1);
		a2.addToDi3(b1);
		a2.addToDi3(b2);

		assertEquals(a1.di3(), Arrays.asList(b1, b2, b3, b1));
		assertEquals(a2.di3(), Arrays.asList(b2, b1, b2));
		assertEquals(a3.di3(), Arrays.asList());

		a1.removeFromDi3(b1);
		a2.removeFromDi3(b2);

		assertEquals(a1.di3(), Arrays.asList(b2, b3, b1));
		assertEquals(a2.di3(), Arrays.asList(b1, b2));
		assertEquals(a3.di3(), Arrays.asList());
	}


	/**
	 * rel A.bi1 <-> B.bi1;
	 */
	private void testBi1() {
		testBi11();
		testBi12();
	}
	private void testBi11() {
		// Init
		setup();
		a1.setBi1(b1);
		a2.setBi1(b2);

		// Change
		a2.setBi1(b1);

		assertNull(a1.bi1());
		assertSame(a2.bi1(), b1);
		assertSame(b1.bi1(), a2);
		assertNull(b2.bi1());
	}
	private void testBi12() {
		// Init
		setup();
		a1.setBi1(b2);

		// Change
		a2.setBi1(b2);

		assertNull(a1.bi1());
		assertSame(a2.bi1(), b2);
		assertNull(b1.bi1());
		assertSame(b2.bi1(), a2);
	}



	/**
	 * rel A.bi2 <-> B.bi2?;
	 */
	private void testBi2() {
		testBi21();
		testBi22();
	}
	private void testBi21() {
		// Init
		setup();
		a1.setBi2(b1);
		a2.setBi2(b2);

		// Change
		a2.setBi2(b1);

		assertNull(a1.bi2());
		assertSame(a2.bi2(), b1);
		assertSame(b1.bi2(), a2);
		assertNull(b2.bi2());
	}
	private void testBi22() {
		// Init
		setup();
		a1.setBi2(b2);

		// Change
		a2.setBi2(b2);

		assertNull(a1.bi2());
		assertSame(a2.bi2(), b2);
		assertNull(b1.bi2());
		assertSame(b2.bi2(), a2);
	}



	/**
	 * rel A.bi3 <-> B.bi3*;
	 */
	private void testBi3() {
		setup();
		a2.setBi3(b2);

		assertNull(a1.bi3());
		assertSame(a2.bi3(), b2);
		assertEquals(b1.bi3(), Arrays.asList());
		assertEquals(b2.bi3(), Arrays.asList(a2));
		assertEquals(b3.bi3(), Arrays.asList());

		a2.setBi3(b3);

		assertNull(a1.bi3());
		assertSame(a2.bi3(), b3);
		assertEquals(b1.bi3(), Arrays.asList());
		assertEquals(b2.bi3(), Arrays.asList());
		assertEquals(b3.bi3(), Arrays.asList(a2));

		a1.setBi3(b3);
		a3.setBi3(b3);

		assertSame(a1.bi3(), b3);
		assertSame(a2.bi3(), b3);
		assertSame(a3.bi3(), b3);
		assertEquals(b1.bi3(), Arrays.asList());
		assertEquals(b2.bi3(), Arrays.asList());
		assertEquals(b3.bi3(), Arrays.asList(a2, a1, a3));

		a2.setBi3(b1);

		assertSame(a1.bi3(), b3);
		assertSame(a2.bi3(), b1);
		assertSame(a3.bi3(), b3);
		assertEquals(b1.bi3(), Arrays.asList(a2));
		assertEquals(b2.bi3(), Arrays.asList());
		assertEquals(b3.bi3(), Arrays.asList(a1, a3));

		try {
			a2.setBi3(null);
			assertException();
		} catch (Exception e) {
			// OK
		}
	}



	/**
	 * rel A.bi4? <-> B.bi4;
	 */
	private void testBi4() {
		testBi41();
		testBi42();
	}
	private void testBi41() {
		// Init
		setup();
		a1.setBi4(b1);
		a2.setBi4(b2);

		// Change
		a2.setBi4(b1);

		assertNull(a1.bi4());
		assertSame(a2.bi4(), b1);
		assertSame(b1.bi4(), a2);
		assertNull(b2.bi4());
	}
	private void testBi42() {
		// Init
		setup();
		a1.setBi4(b2);

		// Change
		a2.setBi4(b2);

		assertNull(a1.bi4());
		assertSame(a2.bi4(), b2);
		assertNull(b1.bi4());
		assertSame(b2.bi4(), a2);
	}



	/**
	 * rel A.bi5? <-> B.bi5?;
	 */
	private void testBi5() {
		testBi51();
		testBi52();
	}
	private void testBi51() {
		// Init
		setup();
		a1.setBi5(b1);
		a2.setBi5(b2);

		// Change
		a2.setBi5(b1);

		assertNull(a1.bi5());
		assertSame(a2.bi5(), b1);
		assertSame(b1.bi5(), a2);
		assertNull(b2.bi5());
	}
	private void testBi52() {
		// Init
		setup();
		a1.setBi5(b2);

		// Change
		a2.setBi5(b2);

		assertNull(a1.bi5());
		assertSame(a2.bi5(), b2);
		assertNull(b1.bi5());
		assertSame(b2.bi5(), a2);
	}



	/**
	 * rel A.bi6? <-> B.bi6*;
	 */
	private void testBi6() {
		setup();
		a2.setBi6(b2);

		assertNull(a1.bi6());
		assertSame(a2.bi6(), b2);
		assertEquals(b1.bi6(), Arrays.asList());
		assertEquals(b2.bi6(), Arrays.asList(a2));
		assertEquals(b3.bi6(), Arrays.asList());

		a2.setBi6(b3);

		assertNull(a1.bi6());
		assertSame(a2.bi6(), b3);
		assertEquals(b1.bi6(), Arrays.asList());
		assertEquals(b2.bi6(), Arrays.asList());
		assertEquals(b3.bi6(), Arrays.asList(a2));

		a1.setBi6(b3);
		a3.setBi6(b3);

		assertSame(a1.bi6(), b3);
		assertSame(a2.bi6(), b3);
		assertSame(a3.bi6(), b3);
		assertEquals(b1.bi6(), Arrays.asList());
		assertEquals(b2.bi6(), Arrays.asList());
		assertEquals(b3.bi6(), Arrays.asList(a2, a1, a3));

		a2.setBi6(b1);

		assertSame(a1.bi6(), b3);
		assertSame(a2.bi6(), b1);
		assertSame(a3.bi6(), b3);
		assertEquals(b1.bi6(), Arrays.asList(a2));
		assertEquals(b2.bi6(), Arrays.asList());
		assertEquals(b3.bi6(), Arrays.asList(a1, a3));

		a2.clearBi6();

		assertSame(a1.bi6(), b3);
		assertNull(a2.bi6());
		assertSame(a3.bi6(), b3);
		assertEquals(b1.bi6(), Arrays.asList());
		assertEquals(b2.bi6(), Arrays.asList());
		assertEquals(b3.bi6(), Arrays.asList(a1, a3));

		assertTrue(a1.hasBi6());
		assertFalse(a2.hasBi6());
		assertTrue(a3.hasBi6());
	}



	/**
	 * rel A.bi7* <-> B.bi7;
	 */
	private void testBi7() {
		setup();
		a2.addToBi7(b2);

		assertEquals(a1.bi7(), Arrays.asList());
		assertEquals(a2.bi7(), Arrays.asList(b2));
		assertNull(b1.bi7());
		assertSame(b2.bi7(), a2);
		assertNull(b3.bi7());

		a2.addToBi7(b3);
		a1.addToBi7(b2);

		assertEquals(a1.bi7(), Arrays.asList(b2));
		assertEquals(a2.bi7(), Arrays.asList(b3));
		assertNull(b1.bi7());
		assertSame(b2.bi7(), a1);
		assertSame(b3.bi7(), a2);

		a1.addToBi7(b1);

		assertEquals(a1.bi7(), Arrays.asList(b2, b1));
		assertEquals(a2.bi7(), Arrays.asList(b3));
		assertSame(b1.bi7(), a1);
		assertSame(b2.bi7(), a1);
		assertSame(b3.bi7(), a2);

		a1.addToBi7(b1);

		assertEquals(a1.bi7(), Arrays.asList(b2, b1));
		assertEquals(a2.bi7(), Arrays.asList(b3));
		assertSame(b1.bi7(), a1);
		assertSame(b2.bi7(), a1);
		assertSame(b3.bi7(), a2);

		a1.removeFromBi7(b1);

		assertEquals(a1.bi7(), Arrays.asList(b2));
		assertEquals(a2.bi7(), Arrays.asList(b3));
		assertNull(b1.bi7());
		assertSame(b2.bi7(), a1);
		assertSame(b3.bi7(), a2);
	}



	/**
	 * rel A.bi8* <-> B.bi8?;
	 */
	private void testBi8() {
		setup();
		a2.addToBi8(b2);

		assertEquals(a1.bi8(), Arrays.asList());
		assertEquals(a2.bi8(), Arrays.asList(b2));
		assertNull(b1.bi8());
		assertSame(b2.bi8(), a2);
		assertNull(b3.bi8());

		a2.addToBi8(b3);
		a1.addToBi8(b2);

		assertEquals(a1.bi8(), Arrays.asList(b2));
		assertEquals(a2.bi8(), Arrays.asList(b3));
		assertNull(b1.bi8());
		assertSame(b2.bi8(), a1);
		assertSame(b3.bi8(), a2);

		a1.addToBi8(b1);

		assertEquals(a1.bi8(), Arrays.asList(b2, b1));
		assertEquals(a2.bi8(), Arrays.asList(b3));
		assertSame(b1.bi8(), a1);
		assertSame(b2.bi8(), a1);
		assertSame(b3.bi8(), a2);

		a1.addToBi8(b1);

		assertEquals(a1.bi8(), Arrays.asList(b2, b1));
		assertEquals(a2.bi8(), Arrays.asList(b3));
		assertSame(b1.bi8(), a1);
		assertSame(b2.bi8(), a1);
		assertSame(b3.bi8(), a2);

		a1.removeFromBi8(b1);

		assertEquals(a1.bi8(), Arrays.asList(b2));
		assertEquals(a2.bi8(), Arrays.asList(b3));
		assertNull(b1.bi8());
		assertSame(b2.bi8(), a1);
		assertSame(b3.bi8(), a2);
	}



	/**
	 * rel A.bi9* <-> B.bi9*;
	 */
	private void testBi9() {
		setup();
		a1.addToBi9(b1);
		a1.addToBi9(b2);

		assertEquals(a1.bi9(), Arrays.asList(b1, b2));
		assertEquals(a2.bi9(), Arrays.asList());
		assertEquals(a3.bi9(), Arrays.asList());
		assertEquals(b1.bi9(), Arrays.asList(a1));
		assertEquals(b2.bi9(), Arrays.asList(a1));
		assertEquals(b3.bi9(), Arrays.asList());
	
		b3.addToBi9(a1);
		b3.addToBi9(a3);
		b3.addToBi9(a1);

		assertEquals(a1.bi9(), Arrays.asList(b1, b2, b3, b3));
		assertEquals(a2.bi9(), Arrays.asList());
		assertEquals(a3.bi9(), Arrays.asList(b3));
		assertEquals(b1.bi9(), Arrays.asList(a1));
		assertEquals(b2.bi9(), Arrays.asList(a1));
		assertEquals(b3.bi9(), Arrays.asList(a1, a3, a1));

		b3.removeFromBi9(a1);

		assertEquals(a1.bi9(), Arrays.asList(b1, b2, b3));
		assertEquals(a2.bi9(), Arrays.asList());
		assertEquals(a3.bi9(), Arrays.asList(b3));
		assertEquals(b1.bi9(), Arrays.asList(a1));
		assertEquals(b2.bi9(), Arrays.asList(a1));
		assertEquals(b3.bi9(), Arrays.asList(a3, a1));
	}


	public void testImmutableList() {
		setup();

		a1.addToDi3(b1);
		a1.addToDi3(b2);
		try {
			a1.di3().add(b3);
			assertException();
		} catch (Exception e) {
			// OK
		}

		a1.addToBi7(b1);
		a1.addToBi7(b2);
		try {
			a1.bi7().add(b3);
			assertException();
		} catch (Exception e) {
			// OK
		}

		a1.addToBi9(b1);
		a1.addToBi9(b2);
		try {
			a1.bi9().add(b3);
			assertException();
		} catch (Exception e) {
			// OK
		}
	}

	private void setup() {
		r = new Root();
		a1 = new A("a1");
		a2 = new A("a2");
		a3 = new A("a3");
		b1 = new B("b1");
		b2 = new B("b2");
		b3 = new B("b3");

		r.addA(a1);
		r.addA(a2);
		r.addA(a3);
		r.addB(b1);
		r.addB(b2);
		r.addB(b3);
	}
}