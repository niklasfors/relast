# RelAST Preprocessor

Run preprocessor on train benchmark (output written to standard output):

	$ ant jar
	$ cat examples/TrainBenchmark.relast
	$ java -jar relast-compiler.jar examples/TrainBenchmark.relast

Run preprocessor and write output to files:

	$ java -jar relast-compiler.jar examples/TrainBenchmark.relast --file
	$ cat examples/TrainBenchmarkGen.ast
	$ cat examples/TrainBenchmarkGen.jadd

Run test cases:

	$ cd test
	$ make

Supported relations:

	// Directed relations
	A.b    -> B;
	A.b?   -> B;
	A.bs*  -> B;

	// Bidirectional relations
	A.b   <-> B.a;
	A.b   <-> B.a?;
	A.b   <-> B.as*;
	A.b?  <-> B.a;
	A.b?  <-> B.a?;
	A.b?  <-> B.as*;
	A.bs* <-> B.a;
	A.bs* <-> B.a?;
	A.bs* <-> B.as*;
